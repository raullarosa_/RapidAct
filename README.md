## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Run locally

1. Open up your terminal in your favorite project directory
2. Make a copy of the remote repository
``` bash
git clone https://gitlab.com/RaulCodes/RapidAct.git
```

3. Switch to the development branch
``` bash
git checkout development
```

4. Create your own branch for your work
``` bash
git checkout -b {INITIALS}-feature/{FEATURE_NAME}
```

5. Import the necessary dependencies
``` bash
npm install
```

6. Enjoy viewing the project locally at localhost:8080 or the specified URL
``` bash
gulp
```

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.